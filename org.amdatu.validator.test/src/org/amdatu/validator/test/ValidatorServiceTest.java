package org.amdatu.validator.test;

import static org.amdatu.testing.configurator.TestConfigurator.*;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import org.amdatu.validator.ValidatorService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ValidatorServiceTest {
	private volatile ValidatorService m_validator;
	
	
	@Before
	public void setup() {
		configure(this).add(createServiceDependency().setService(ValidatorService.class).setRequired(true)).apply();
	}
	
	@Test(expected=ConstraintViolationException.class)
	public void testInvalid() {
		m_validator.validate(new ObjectToValidate(null, null));
	}		
	
	@Test(expected=ConstraintViolationException.class)
	public void testInvalidOptional() {
		m_validator.validate(new ObjectToValidate(null, "ab"));
	}		
	
	@Test
	public void testValid() {
		m_validator.validate(new ObjectToValidate("test", null));
		m_validator.validate(new ObjectToValidate("test", "abc"));
	}
	
	@Test
	public void testGetUnderlyingValidator() {
		Validator validator = m_validator.getValidator();
		assertNotNull(validator);
	}
	
	@Test
	public void testUseUnderlyingValidator() {
		Validator validator = m_validator.getValidator();
		Set<ConstraintViolation<ObjectToValidate>> result = validator.validate(new ObjectToValidate(null, null));
		assertFalse(result.isEmpty());
		
		Set<ConstraintViolation<ObjectToValidate>> result2 = validator.validate(new ObjectToValidate("test", "abc"));
		assertTrue(result2.isEmpty());
	}
	
	@Test(expected=ConstraintViolationException.class)
	public void testUseHibernateSpecificConstraint() {
		m_validator.validate(new HibernateAnnotatedObject("", "abc"));
	}
	
	@Test
	public void testDefaultErrorMessage() {
		try {
			m_validator.validate(new ObjectToValidate(null, null));
		} catch(ConstraintViolationException ex) {
			ConstraintViolation<?> violation = ex.getConstraintViolations().iterator().next();
			String message = violation.getMessage();
			assertEquals("m_name may not be null", violation.getPropertyPath() + " " + message);
		}
	}
	
	@After
	public void cleanup() {
		cleanUp(this);
	}
}