/*
 * Copyright (c) 2015 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.validator.rest.example;

import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.amdatu.validator.ValidatorService;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

public class ValidatingRestApplication extends Application {

	private volatile Set<Object> m_singletons;

	private volatile ValidatorService m_validatorService;

	@SuppressWarnings("unused" /* dependency manager callback */)
	private void start() {
		m_singletons = new HashSet<Object>();
		m_singletons.add(new ValidatingJacksonJsonProvider(m_validatorService));
	}

	@Override
	public Set<Object> getSingletons() {
		return m_singletons;
	}

	class ValidatingJacksonJsonProvider extends JacksonJsonProvider {

		private ValidatorService m_validatorService;

		public ValidatingJacksonJsonProvider(ValidatorService validatorService) {
			m_validatorService = validatorService;
		}

		@Override
		public Object readFrom(Class<Object> arg0, Type arg1, Annotation[] arg2, MediaType arg3, 
				MultivaluedMap<String, String> arg4, InputStream arg5) throws IOException, WebApplicationException {

			Object object = super.readFrom(arg0, arg1, arg2, arg3, arg4, arg5);

			try {
				m_validatorService.validate(object);
			} catch (ConstraintViolationException e) {
				
				List<ConstraintViolationResponseWrapper> messages = new ArrayList<>();
				for (ConstraintViolation<?> c : e.getConstraintViolations()) {
					messages.add(new ConstraintViolationResponseWrapper(c));
				}
				throw new WebApplicationException(Response.status(Status.BAD_REQUEST).entity(messages).build());
			}

			return object;
		}
	}

	class ConstraintViolationResponseWrapper {
		private ConstraintViolation<?> m_constraintViolation;

		public ConstraintViolationResponseWrapper(ConstraintViolation<?> constraintViolation) {
			m_constraintViolation = constraintViolation;
		}
		
		@JsonGetter
		public String getProperty(){
			return m_constraintViolation.getPropertyPath().toString();
		}
		
		@JsonGetter
		public String getMessage(){
			return m_constraintViolation.getMessage();
		}
	}
	
}
