package org.amdatu.validator.rest.example;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ObjectToValidate {

	@NotNull(message="test")
	private String m_name;

	@Size(min=4)
	private String m_password; 
	
	@JsonCreator
	public ObjectToValidate(@JsonProperty("name") String name, @JsonProperty("password") String password) {
		m_name = name;
		m_password = password;
	}
	
	public String getName() {
		return m_name;
	}

	public void setName(String name) {
		m_name = name;
	} 
	
	public String getPassword(){
		return m_password;
	}
	
	public void setPassword(String password){
		m_password = password;
	}
	
}
