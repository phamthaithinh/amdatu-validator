package org.amdatu.validator.rest.example;

import java.util.Properties;

import javax.ws.rs.core.Application;

import org.amdatu.validator.ValidatorService;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;

public class Activator extends DependencyActivatorBase {

	@Override
	public void destroy(BundleContext arg0, DependencyManager arg1)
			throws Exception {

	}

	@Override
	public void init(BundleContext arg0, DependencyManager dm) throws Exception {

		Properties applicationProps = new Properties();
		applicationProps.put(Constants.SERVICE_RANKING, 1);
		dm.add(createComponent()
				.setInterface(Application.class.getName(), applicationProps)
				.setImplementation(ValidatingRestApplication.class)
				.add(createServiceDependency().setService(
						ValidatorService.class).setRequired(true)));

		dm.add(createComponent().setInterface(Object.class.getName(), null)
				.setImplementation(HelloValidatorResource.class));

	}

}
