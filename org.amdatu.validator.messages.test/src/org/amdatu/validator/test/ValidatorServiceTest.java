/*
 * Copyright (c) 2015 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.validator.test;

import static org.amdatu.testing.configurator.TestConfigurator.cleanUp;
import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.createServiceDependency;
import static org.junit.Assert.assertEquals;

import javax.validation.ConstraintViolationException;

import org.amdatu.validator.ValidatorService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ValidatorServiceTest {
	private volatile ValidatorService m_validator;
	
	@Before
	public void setup() {
		configure(this).add(createServiceDependency().setService(ValidatorService.class).setRequired(true)).apply();
	}
	
	@Test
	public void testDefaultErrorMessage() {
		try {
			m_validator.validate(new ObjectToValidate(null));
		} catch(ConstraintViolationException ex) {
			String message = ex.getConstraintViolations().iterator().next().getMessage();
			assertEquals("May not be null.", message);
		}
	}
	
	@After
	public void cleanup() {
		cleanUp(this);
	}
}
