/*
 * Copyright (c) 2015 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.validator.beanvalidation;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.amdatu.validator.ValidatorService;

public class BeanValidationImpl implements ValidatorService {
	private volatile Validator m_validator;
	
	public void start() {
		ValidatorFactory validatorFactory = Validation.byDefaultProvider()
		        .providerResolver( new OsgiServiceDiscoverer() )
		        .configure()
		        .buildValidatorFactory();
		m_validator = validatorFactory.getValidator();
	}

	@Override
	public <T> void validate(T objectToValidate) {
		ClassLoader ccl = Thread.currentThread().getContextClassLoader();
		
		try {
			Thread.currentThread().setContextClassLoader(this.getClass().getClassLoader());
			Set<ConstraintViolation<T>> validate = m_validator.validate(objectToValidate);
			if(!validate.isEmpty()) {
				throw new ConstraintViolationException(validate);
			}
		} finally {
			Thread.currentThread().setContextClassLoader(ccl);
		}
	}

	@Override
	public Validator getValidator() {
		return m_validator;
	}
}
