package org.amdatu.validator.test;

import java.util.Optional;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ObjectToValidate {

	@NotNull
	private String m_name;

	@Size(min=3)
	private Optional<String> m_optionalValue;
	
	public ObjectToValidate(String name, String optionalValue) {
		m_name = name;
		setOptional(optionalValue != null ? Optional.of(optionalValue) : Optional.empty());
	}

	public String getName() {
		return m_name;
	}

	public void setName(String name) {
		m_name = name;
	}

	public Optional<String> getOptional() {
		return m_optionalValue;
	}

	public void setOptional(Optional<String> m_optionalValue) {
		this.m_optionalValue = m_optionalValue;
	}
}
