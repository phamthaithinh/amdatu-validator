# Introduction #

This project makes the Hibernate Validator available to OSGi. It currently has the following features:

1. Wrap Hibernate Validator and its dependencies in a single bundle
2. Provide an OSGi service to trigger validation
3. Support for custom error messages using fragment bundles

User docs can be found on the [website](http://amdatu.org/components/validator.html).

## Building

This project uses [Gradle](http://www.gradle.org) as build system. To build this project from source, you only need to
invoke `gradle build`, or use the included wrapper: `./gradlew build`. The resulting bundles can be found in the
`org.amdatu.validator/generated` directory.

## Links

* [Amdatu Website](http://www.amdatu.org/components/validator.html);
* [Source Code](https://bitbucket.org/amdatu/amdatu-validator);
* [Issue Tracking](https://amdatu.atlassian.net/projects/VALIDATOR);
* [Development Wiki](https://amdatu.atlassian.net/wiki/display/AMDATUDEV/Amdatu+Validator);
* [Continuous Build](https://amdatu.atlassian.net/builds/browse/VALIDATOR-MAIN).
